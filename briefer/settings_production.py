# -*- coding: utf-8 -*-
# Load settings first
try:
    from settings import *
except ImportError:
    pass

# Now override any of them
LOCAL = False
DEBUG = False
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'briefer',
        'USER': 'briefer',
        'PASSWORD': 'WqET7bAJojqbTLg',
        'HOST': '',
        'PORT': '',
    }
}

#Configurations of the tools
ADMIN_TOOLS_MENU = 'briefer.briefer.menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'briefer.briefer.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'briefer.briefer.dashboard.CustomAppIndexDashboard'


ROOT_URLCONF = 'briefer.briefer.urls'