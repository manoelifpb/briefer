# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *

urlpatterns = patterns('homepage.views',
    url(r'^$', 'index', name="index"),
    url(r'^cadastrar-newsletter/$', 'cadastrar_newsletter', name="cadastrar-newsletter"),
)