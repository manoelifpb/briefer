# -*- coding:utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.template import RequestContext, loader
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json

from models import *
from forms import NewsletterForm


def index(request, template_name="homepage/index.html"):

    return render_to_response(template_name, {
    },context_instance=RequestContext(request))


def cadastrar_newsletter(request):
    response_data = {}

    if request.is_ajax() and request.method == 'POST':
        form = NewsletterForm(request.POST)
        if form.is_valid():
            if Newsletter.objects.filter( email=form.cleaned_data['email'] ).count() > 0:
                response_data['status'] = 'already-registred'
            else:
                response_data['status'] = 'ok'
                form.save()
        else:
            response_data['status'] = 'error'

        return HttpResponse(json.dumps(response_data), content_type="application/json")

    else:
        return HttpResponseRedirect('/')