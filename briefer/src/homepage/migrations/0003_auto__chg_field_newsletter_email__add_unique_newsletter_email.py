# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Newsletter.email'
        db.alter_column(u'homepage_newsletter', 'email', self.gf('django.db.models.fields.EmailField')(max_length=255, unique=True, null=True))
        # Adding unique constraint on 'Newsletter', fields ['email']
        db.create_unique(u'homepage_newsletter', ['email'])


    def backwards(self, orm):
        # Removing unique constraint on 'Newsletter', fields ['email']
        db.delete_unique(u'homepage_newsletter', ['email'])


        # Changing field 'Newsletter.email'
        db.alter_column(u'homepage_newsletter', 'email', self.gf('django.db.models.fields.EmailField')(max_length=512, null=True))

    models = {
        u'homepage.newsletter': {
            'Meta': {'object_name': 'Newsletter'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['homepage']