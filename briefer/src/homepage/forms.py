# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _

from models import Newsletter



class NewsletterForm(forms.ModelForm):
    class Meta:
        model = Newsletter