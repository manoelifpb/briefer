# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext as _

from models import *


class NewsletterAdmin(admin.ModelAdmin):
    list_display = ('email',  )
    search_fields = ('email', )
    save_on_top = True

    fieldsets = [
        (_(u'Newsletter'),                   {'fields' : ('email',), }, ),
    ]

admin.site.register(Newsletter, NewsletterAdmin)