# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from django.db.models import signals


class Newsletter(models.Model):
    email = models.EmailField(u'Email', max_length=255, blank=True, null=True, unique=True)

    def __unicode__(self):
        return self.email

    class Meta:
        verbose_name = u'Newsletter'
        verbose_name_plural = u'Newsletter'