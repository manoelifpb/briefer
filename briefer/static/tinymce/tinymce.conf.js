tinymce.init({
    selector: "textarea",
    theme: "modern",
    height: 300,
    language : 'pt_BR',
    statusbar : false,
    plugins: [
         "advlist autolink link lists charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
    ],
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | print preview fullpage | forecolor backcolor emoticons", 
 }); 