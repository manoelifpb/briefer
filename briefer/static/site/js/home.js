(function(){

// preload mao

var maoImg = new Image();
maoImg.onload = function() {
    $('#mao').addClass('animate');
}
maoImg.src = '/static/site/images/mao.png';

}());

var resizer = (function(){

    var main = $('.main'),
        wrapper = $(window),
        proportion = 1920 / 960;

    function update() {
        // tenta pegar a proporção da imagem, se não, é assumível que o servidor vai 
        // entregar em 1920 / 1080

        var newWidth, newHeight, newTop, newLeft,
            blockWidth, blockHeight;

        blockWidth = wrapper.width();
        blockHeight = wrapper.height();

        newWidth = blockWidth;
        newHeight = newWidth / proportion;

        if ( newHeight < blockHeight ) {
            newHeight = blockHeight;
            newWidth = newHeight * proportion;
        }

        newLeft = ( blockWidth - newWidth ) / 2;
        newTop = ( blockHeight - newHeight ) / 2;

       main.css({
            width : newWidth + 'px',
            height : newHeight + 'px',
            top : newTop + 'px',
            left : newLeft + 'px'
        });

    }

    function init() {
        update();

        $(window).resize(update);
    }

    return {
        update : update,
        init: init
    }
}());

resizer.init();

var fonts = (function(){

    var main = $('.main'),
        width = 1920, font = 100;

    function update() {
        var newFont = ( main.width() / width ) * font;
        main.css('font-size', newFont + 'px');
    }

    function init() {
        update();
        $(window).resize(update);
    }

    return {
        update : update,
        init: init
    }
}());

fonts.init();