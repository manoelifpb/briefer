/*

    18/05
        Adicionado beforeAnimate
*/



(function($){

    var 

        default_options = {
            slide_obj : '.slide_obj'
        },

        toString = Object.prototype.toString;



    function MouseMoveSlider(father, options) {
       

        return this;
    };

    MouseMoveSlider.prototype = {
        init : function () {

        }
    };

    var methods = {
        init : function ( options ) {
            return this.each(function(){
                $(this).data('mouseMoveSlider', new MouseMoveSlider(this, options));
            });
        },
        update : function() {
            var slider =  this.data('mouseMoveSlider');
            slider.update();
        }
    }

    $.fn.mouseMoveSlider = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.mouseMoveSlider' );
        } 

    };

})(window.jQuery);
