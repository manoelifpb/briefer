// modulo de urls, usado para delegar ação quando chegar a uma url
// igual ao do django
// por Manoel Quirino Neto



(function(){
    var w = window,
        d = document;

    // padrões salvos

    var patterns = [];

    // define um padrão para url
    function UrlPattern(regex, callback, reverse) {

        this.regex = regex;
        this.callback = callback;
        this.reverse = reverse || false;

        return this;

    }

    function test( url ) {
        // var resto = ''
        // try{
        //     resto = /(\?.*)$/.exec( url )[1];
        //     url = /^([^\?]*)/.exec( url )[1];
        // } catch(err) {
        // }

        for ( var i = 0, k = patterns.length; i < k; i++ ) {
            var patter = patterns[i];

            if ( ! patter.reverse ) {
                if ( patter.regex.test( url ) ) {
                    if ( patter.callback.apply(patter, patter.regex.exec(url) ) ) {
                        return true
                    }
                }
            } else {
                if ( ! patter.regex.test( url ) ) {
                    if ( patter.callback.apply(patter, patter.regex.exec(url) ) ) {
                        return true
                    }
                }
            }
        }

        return false;
    }

    function trigger() {
        var url = History.getState().url;
        test( url );
    }

    function push( pattern ) {
        patterns.push( pattern );
    }

    w.urls = {
        UrlPattern : UrlPattern,
        push : push,
        trigger: trigger,
        test: test
    };

}());