/*

    18/05
        Adicionado beforeAnimate
*/



(function($){

    var 

        default_options = {
            timeout : 8000,
            animation_time : 400,
            current : 0,

            timing : 'swing',
            slideObjs : '.slide_obj',
            // box : '.slide_box',
            paginatorActive : 'active',
            slideActive : 'active',

            paginator : null,
            prevPaginator : null,
            nextPaginator : null,

            hide_paginator_if_has_little : true,

            beforeAnimate : function(){}
        },

        toString = Object.prototype.toString;



    function FadeSlider(father, options) {
        var 
            that = this,
            opt = $.extend({}, default_options, options);

        this.timeout = opt['timeout'];
        this.father = $(father);
        this.slideObjs = this.father.find( opt['slideObjs'] );
        // this.box = this.father.find( opt['box'] );
        this.timing = this.father.find( opt['timing'] );
        this.current = parseInt( opt['current'] );
        this.paginatorActive = opt['paginatorActive'];
        this.hide_paginator_if_has_little = opt['hide_paginator_if_has_little'];
        this.slideActive = opt['slideActive'];
        this.beforeAnimate = opt['beforeAnimate'];
        this.animation_time = parseInt( opt['animation_time'] );
        this.childCount = this.slideObjs.length;



        // testa se tem paginator
        if ( toString.call(opt['paginator']) === '[object String]' && opt['paginator'] !== '' ) {
            this.paginator = this.father.find( opt['paginator']);
            this.paginator.each(function(index){
                $(this).attr('data-index', index).
                    click(function(e){
                        if ( e.preventDefault ) {
                            e.preventDefault();
                            e.stopPropagation();
                        }

                        that.set( parseInt($(this).attr('data-index')) );

                        return false;
                    });
            });
        }

        // testa se tem prev and next
        if ( toString.call(opt['prevPaginator']) === '[object String]' && opt['prevPaginator'] !== '' ) {
            this.prevPaginator = this.father.find( opt['prevPaginator'] );
            this.prevPaginator.click(function(e){
                if ( e.preventDefault ) {
                    e.preventDefault();
                    e.stopPropagation();
                }

                that.prev();

                return false;
            });
        }
        if ( toString.call(opt['nextPaginator']) === '[object String]' && opt['nextPaginator'] !== '' ) {
            this.nextPaginator = this.father.find( opt['nextPaginator'] );
            this.nextPaginator.click(function(e){
                if ( e.preventDefault ) {
                    e.preventDefault();
                    e.stopPropagation();
                }

                that.next();

                return false;
            });
        }

        if ( this.slideObjs.length < 2 ) {

            if ( this.hide_paginator_if_has_little ) {
                
                if ( this.nextPaginator ) {
                    this.nextPaginator.hide();
                }

                if ( this.prevPaginator ) {
                    this.prevPaginator.hide();
                }

                if ( this.paginator ) {
                    this.paginator.hide();
                }
            }

            return false;
        };


        this.init();

        return this;
    };

    FadeSlider.prototype = {
        init : function () {
            this.remakeTimeout();
            this.slideObjs.show().hide().css({
                position: 'absolute',
                top : 0,
                left : 0
            }).eq( this.current ).show();

            this.changePaginator( this.current );

        }

        , remakeTimeout : function() {
            var that = this;
            this.stopTimeout();
            this.timeout_obj = setInterval( function(){
                that.next();
            } , this.timeout );
        }

        , stopTimeout : function() {
            clearInterval( this.timeout_obj );
        }

        , next : function() {
            this.remakeTimeout();
            this.current++;
            if ( this.current >= this.childCount ) {
                this.current = 0;
            }
            this.animate();
        }

        , prev : function() {
            this.remakeTimeout();
            this.current--;
            if ( this.current < 0 ) {
                this.current = this.childCount - 1;
            }
            this.animate();
        }

        , changePaginator : function( number ) {
            if ( this.paginator && this.paginator.length && this.paginator.length > 1 ) {
                this.paginator.removeClass( this.paginatorActive ).
                    eq( number ).addClass( this.paginatorActive );
            }
        }
 
        , set : function( number ) {
            this.current = number;
            this.remakeTimeout();
            this.animate();
        }

        , changeActiveSlideObj : function() {
            var current = this.current;

            this.slideObjs.removeClass( this.slideActive ).
                eq( current ).addClass( this.slideActive );
        }

        , animate : function() {
            var current = this.current;

            this.changePaginator( current );
            this.changeActiveSlideObj();

            this.beforeAnimate(this.slideObjs, this.slideObjs.eq(current));

            this.slideObjs.stop(true, true).fadeOut(this.animation_time)
                .eq( current ).stop(true, true)
                .fadeIn( this.animation_time );
        }

    };

    var methods = {
        init : function ( options ) {
            return this.each(function(){
                $(this).data('fadeSlider', new FadeSlider(this, options));
            });
        },
        stopTimeout : function() {
            var slider =  this.data('fadeSlider');
            slider.stopTimeout();
        },
        remakeTimeout : function() {
            var slider =  this.data('fadeSlider');
            slider.remakeTimeout();
        }
    }

    $.fn.fadeSlider = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.responsiveManySlider' );
        } 

    };

})(window.jQuery);
