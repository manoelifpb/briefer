/*

    18/05
        Adicionado beforeAnimate
*/



(function($){

    var 

        default_options = {
            last : {
                x : 0,
                y : 0
            },
            dontTouch : false,
            callback_touch_start : null,
            callback_touch_move : null,
            callback_touch_end : null,

            callback_update : null
        },

        toString = Object.prototype.toString;


    function getMouseCoords(event) {
        if(event == null){ event = window.event; }
        if(event == null){ return null; }
        if(event.pageX || event.pageY){ return {x:event.pageX, y:event.pageY}; }

        try {
            if ( event.originalEvent && event.originalEvent.touches ) {
              return {x:event.originalEvent.touches[0].pageX, y:event.originalEvent.touches[0].pageY};
            }
        } catch(e) { console.log(e); }

        return null;
    }

    function Touch(obj, options) {
        this.options = $.extend({}, default_options, options);
        this.last = this.options.last;
        this.dontTouch = this.options.dontTouch;
        this.obj = $(obj);

        this.appendListeners();

        return this;
    };

    Touch.prototype = {
        
        appendListeners : function() {
            var that = this;
            that.touching = false;

            var events = {
                touchstart : function(e) {
                    if ( that.dontTouch ) {
                        return true;
                    }

                    if ( e.preventDefault ) {
                        e.preventDefault();
                        e.stopPropagation();
                    }

                    that.touching = true;
                    that.initialCoords = getMouseCoords(e);

                    if ( that.options.callback_touch_start ) {
                        that.options.callback_touch_start.call(this, that);
                    }
                },
                touchmove : function(e) {
                    if ( that.dontTouch ) {
                        return true;
                    }

                    if ( e.preventDefault ) {
                        e.preventDefault();
                        e.stopPropagation();
                    }

                    var nowCoords = getMouseCoords(e);

                    if ( that.touching ) {
                        that.diff = {
                            x : that.initialCoords.x - nowCoords.x,
                            y : that.initialCoords.y - nowCoords.y
                        };

                        that.new = {
                            x : that.last.x - that.diff.x,
                            y : that.last.y - that.diff.y
                        };

                        if ( that.options.callback_update ) {
                            that.options.callback_update.call(this, that.new, that.diff);
                        } else {
                            that.obj.css('-webkit-transform', 'translate3d(' + that.new.x + 'px, ' + that.new.y + 'px, 0)');
                        }
                    }
                    // console.log(e);
                },
                touchend : function(e) {
                    if ( that.dontTouch ) {
                        return true;
                    }

                    if ( e.preventDefault ) {
                        e.preventDefault();
                        e.stopPropagation();
                    }

                    that.last = that.new;
                    that.touching = false;

                    if ( that.options.callback_touch_end ) {
                        that.options.callback_touch_end.call(this, that);
                    }
                }
            }
            this.obj.bind('mousedown', events.touchstart).bind('touchstart', events.touchstart);
            this.obj.bind('mousemove', events.touchmove).bind('touchmove', events.touchmove);
            this.obj.bind('mouseup', events.touchend).bind('touchend', events.touchend);
        }
    };

    var methods = {
        init : function ( options ) {
            return this.each(function(){
                $(this).data('touch', new Touch(this, options));
            });
        },
        getCurrent : function() {
            return $(this).data('touch').last;
        },
        setCurrent : function( current ) {
            $(this).data('touch').last = current;
        }
    }

    $.fn.touch = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.touch' );
        } 

    };

})(window.jQuery);