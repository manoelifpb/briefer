/*

    18/05
        Adicionado beforeAnimate
*/



(function($){

    var 

        default_options = {
            scroll : '.scroll',
            getProportionXShowing : function(){ return 0; },
            getProportionYShowing : function(){ return 0; },
            show : function(){ },
            scrollTop : 0,
            scrollLeft : 0
        },

        toString = Object.prototype.toString;



    function SodaScroll(father, options) {

        this.options = $.extend({}, default_options, options);
        this.father = father;
        this.$scroll = $(father).find( this.options.scroll );
        this.scrollTop = this.options.scrollTop;
        this.scrollLeft = this.options.scrollLeft;

        this.init();

        return this;
    };

    function getMouseCoords(event) {
        if(event == null){ event = window.event; }
        if(event == null){ return null; }
        if(event.pageX || event.pageY){ return {x:event.pageX, y:event.pageY}; }

        try {
            if ( event.originalEvent && event.originalEvent.touches ) {
              return {x:event.originalEvent.touches[0].pageX, y:event.originalEvent.touches[0].pageY};
            }
        } catch(e) { console.log(e); }

        return null;
    }

    SodaScroll.prototype = {
        init : function () {
            var that = this;

            this.appendListeners();

            this.resize();

            var resizeTimeout;
            $(window).resize(function(){

                clearTimeout(resizeTimeout);
                setTimeout(function(){
                    that.resize();
                }, 300);

            });
        },

        appendListeners : function() {
            that = this;

            function mousedown( e ) {
                if ( e.preventDefault ) {
                    e.stopPropagation();
                    e.preventDefault();
                }

                var coords = getMouseCoords(e);
                that.initialCoords = coords;

                that.initialScroll = {
                    x : that.scrollLeft,
                    y : that.scrollTop
                };

                that.scrolling = true;
            }

            function mousemove( e ) {
                if ( that.scrolling ) {   
                    var coords = getMouseCoords(e);
                    if ( that.scrolling ) {
                        var diff = {
                            x : coords.x - that.initialCoords.x,
                            y : coords.y - that.initialCoords.y
                        }

                        that.scrollLeft = Math.max(0, Math.min(that.maxScroll.x, diff.x + that.initialScroll.x) );
                        that.scrollTop = Math.max(0, Math.min(that.maxScroll.y, diff.y + that.initialScroll.y) );
                    }

                    that.show();
                }
            }


            function mouseup( e ) {
                if ( that.scrolling ) {
                    that.scrolling = false;
                }
            }

            this.$scroll.bind('mousedown.soda-scroll', mousedown).bind('touchstart.soda-scroll', mousedown);
            $(document).bind('mousemove.soda-scroll', mousemove).bind('touchmove.soda-scroll', mousemove);
            $(document).bind('mouseup.soda-scroll', mouseup).bind('touchemd.soda-scroll', mouseup);

        },

        resize : function(){
            var opt = this.options;

            this.calculate();
            this.show();
        },

        calculate : function(){
            var opt = this.options;

            var newProportion = {
                x : opt.getProportionXShowing.call(this),
                y : opt.getProportionYShowing.call(this)
            };

            if ( this.proportion ) {
                this.scrollLeft = newProportion.x / this.proportion.x * this.scrollLeft;
            }

            this.scrollWidth = Math.min(1, newProportion.x) * this.father.offsetWidth;
            // implement
            this.scrollHeight = 0;

            this.maxScroll = {
                x : this.father.offsetWidth - this.scrollWidth,
                // inplement
                y : 0
            }

            this.proportion = newProportion;
        },  

        show : function() {

            this.$scroll.css({
                'width' : this.scrollWidth + 'px',
                'margin-left' : this.scrollLeft + 'px'
            });

            this.options.show.call(this);

        }

    };

    var methods = {
        init : function ( options ) {
            return this.each(function(){
                $(this).data('sodaScroll', new SodaScroll(this, options));
            });
        },
        update : function() {
            return this.each(function(){
                var slider =  $(this).data('sodaScroll');
                slider.calculate();
                slider.show();
            });
        },

        setScrollLeft : function( newScrollLeft ) {
            return this.each(function(){
                var slider =  $(this).data('sodaScroll');
                slider.scrollLeft = newScrollLeft;
            });
        }
    }

    $.fn.sodaScroll = function( method ) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.sodaScroll' );
        } 

    };

})(window.jQuery);
