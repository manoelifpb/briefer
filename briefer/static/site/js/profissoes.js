(function(){

    function closeModal() {
        $('#profissao-interna-wrapper').fadeOut(500, function(){
            $(this).remove();
        });
         $('#content, #profissao-interna-wrapper').css({
            height: '',
            paddingBottom: ''
         });
    }

    function showModal( html ) {

        var $html = $(html);

        $html.hide().appendTo('#index').fadeIn();

        $html.find('.close-modal').on('click', function(){
            closeModal();
        });

        if ( window.FB ) {
            FB.XFBML.parse();
        }

        var height = $('#profissao').height();
        var cHeight = $('#content').height();

        if(height>cHeight) {
            $('#content, #profissao-interna-wrapper').css('height', height);
            $('#content').css('padding-bottom', '100px');
            $('#profissao-interna-wrapper').css('padding-bottom', '160px');
        }

    }

    function openProfissao( url ) {
        $.ajax({
            url: url,
            success : function( html ){
                showModal(html);
            },
            error : function() {
                console.log( arguments );
            }
        });
    }

    $('.professor-link').on('click', function(e){

        if (e.preventDefault) {
            e.preventDefault();
            e.stopPropagation();
        }

        openProfissao(this.href);

        return false;
    });

    urls.push( 
        new urls.UrlPattern(/\/eu-faco-o-meu-futuro\/profissao\/(?:[\w_-]+)\//, function( url ){
            openProfissao( url );
        })
    );

    urls.trigger();


}());