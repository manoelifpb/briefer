$(document).ready(function() {
	$('#profissoes').on('click', '.ajax', function(e) {
		e.preventDefault();
		var href = $('.ajax').attr('href');
		$('#modal').load(href);
		$('#modal:before').css('display', 'block');
		return false;
	});
	$('#profissoes').on('click', '.close-modal', function(e) {
		e.preventDefault();
		$('#profissao').remove();
		return false;
	});
});