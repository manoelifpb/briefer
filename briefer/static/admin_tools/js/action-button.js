/* action-button, using to create links buttons on listview */
(function($) {
    $(document).ready(function() {
        $('.action-link').click(function(){
            var object_id = $(this).attr('object-id'),
            action = $(this).attr('action');
            $('input:checkbox[name=_selected_action]').attr('checked', '');
            $('input:checkbox[name=_selected_action][value='+object_id+']').attr('checked', 'checked');
            $('select[name=action]').attr('value', action);
            $('#changelist-form').submit();
            return false;
        });
    });
})(django.jQuery);