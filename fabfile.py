from __future__ import with_statement
from fabric.api import *

env.hosts = [
    'soda@seculo.sodateste.com.br',
]
env.warn_only = True

def deploy():
    with cd('/deploy/sites/seculo/seculo'):
        # get lastest version from git
        run('git pull')
        # restart nginx
        run('../nginx/bin/restart')


def deploy_migrate():
    with cd('/deploy/sites/seculo/seculo'):
        # get lastest version from git
        run('git pull')
        run('../bin/python manage.py syncdb --settings=seculo.settings_production')
        run('../bin/python manage.py migrate --settings=seculo.settings_production')
        # restart nginx
        run('../nginx/bin/restart')